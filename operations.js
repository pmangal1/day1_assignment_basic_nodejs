const add = (n1, n2) => {
  console.log(n1 + n2)
}

const substract = (n1, n2) => {
  console.log(n1 - n2)
}

const multiply = (n1, n2) => {
  console.log(n1 * n2)
}

const divide = (n1, n2) => {
  console.log(n1 / n2)
}

module.exports = { add, substract,  multiply, divide}