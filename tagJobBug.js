const fs = require("fs");
const jobs = require("./data/jobs.json");
const technologies = require("./data/technologies.json")

const processedTechnologiesData = {}

// converted technologies into key(lowercase) value(original) pair 
technologies.map(technology => {
  processedTechnologiesData[technology.toLowerCase()] = technology
})

// processing jobs
for (let i = 0; i < jobs.length; i++) {
  let tags = new Set(); // set for technologies tag
  let processingTimeStarts = new Date().getTime() // time starts
  // splitting description with spaces
  jobs[i].description.split(' ').map(word => {

    // removing begining and end dot if any and coverting to lowercase
    let processedWord = word.replace(/^\.+/, "").replace(/\.+$/, "").toLowerCase();
    if (processedWord in processedTechnologiesData)
      tags.add(processedTechnologiesData[processedWord])

  })
  jobs[i].tags = [...tags].join(', ');
  jobs[i].processing_timestamp = new Date().getTime() - processingTimeStarts; // processing done

}

// Writing jobs and updated tags to a file 
fs.writeFile(`./data/${new Date().getTime()}_response.json`, JSON.stringify(jobs), err => {
  // Checking for errors 
  if (err) throw err;
  console.log("Done writing"); // Success 
});
